﻿namespace Numerics_generator.Core
{
    public sealed class GenerateEventArgs 
    {
        /// <summary>
        /// Value of current iteration.
        /// </summary>
        public double Value { get; }
        

        /// <summary>
        /// Number of iteration (from 0 to N).
        /// </summary>
        public int Iteration { get; }

        public GenerateEventArgs(double value, int iteration)
        {
            Value = value;
            Iteration = iteration;
        }
    }
}
