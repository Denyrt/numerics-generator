﻿using System;
using System.Threading;

namespace Numerics_generator.Core
{
    public sealed class NumberGenerator
    {
        /// <summary>
        /// Min value of the last numerics generation.
        /// </summary>
        public double MinValue { get; private set; } = 1;

        /// <summary>
        /// Max value of the last numerics generation.
        /// </summary>
        public double MaxValue { get; private set; } = 0;

        /// <summary>
        /// Length for numerics generation.
        /// </summary>
        public int NumericsLength { get; set; }

        #region Events

        /// <summary>
        /// Called when value is generated.
        /// </summary>
        public event EventHandler<GenerateEventArgs> NumberGenerated;

        /// <summary>
        /// Called when numerics genetarion is finished.
        /// </summary>
        public event EventHandler GenerationFinished;

        #endregion

        public NumberGenerator()
        {

        }

        public void RefreshMinValue() => MinValue = 1;

        public void RefreshMaxValue() => MaxValue = 0;

        public bool ValidateModel(out string message)
        { 
            if (NumericsLength < 1)
            {
                message = "Numerics Length cannot be lesser than 1.";
                return false;
            }

            message = string.Empty;
            return true;
        }

        public void Generate() => Generate(default);

        public void Generate(CancellationToken token)
        {
            var rnd = new Random(DateTime.Now.GetHashCode());
            
            for (int i = 0; i < NumericsLength; ++i)
            {
                if (token.IsCancellationRequested)
                {
                    GenerationFinished?.Invoke(this, new EventArgs());
                    token.ThrowIfCancellationRequested();
                }               
                
                var val = rnd.NextDouble();

                if (val < MinValue) MinValue = val;
                if (val > MaxValue) MaxValue = val;

                NumberGenerated?.Invoke(this, new GenerateEventArgs(val, i));
            }

            GenerationFinished?.Invoke(this, new EventArgs());
        }
    }
}
