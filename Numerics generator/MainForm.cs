﻿using Numerics_generator.Core;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Numerics_generator
{
    public partial class MainForm : Form
    {
        private CancellationTokenSource _cts;
        private NumberGenerator _generator;

        public MainForm()
        {
            InitializeComponent();
            _generator = new NumberGenerator();

            _generator.NumberGenerated += InsertIntoTextBox;
            _generator.NumberGenerated += InsertIntoDataGrid;            
            _generator.GenerationFinished += OnGenerationFinished;
        }        

        private void ToolStripMenuItemClear_Click(object sender, EventArgs e)
        {
            richTextBox.Clear();
        }

        private void BeforeStartPrepare()
        {
            _generator.RefreshMinValue();
            _generator.RefreshMaxValue();

            progressBar.Value = 0;
            progressBar.Maximum = _generator.NumericsLength;

            _cts?.Dispose();
            _cts = new CancellationTokenSource();            
            _cts.Token.Register(CancellationCallback);

            buttonStart.Enabled = false;
            buttonCancel.Enabled = true;

            richTextBox.Clear();
            dataGridView.Rows.Clear();

            for (int i = dataGridView.ColumnCount - 1; i > 0 ; --i)
            {
                dataGridView.Columns.RemoveAt(i);
            }            
        }
        

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            int.TryParse(textBoxNumCount.Text, out var count);

            _generator.NumericsLength = count;

            #region Validation

            if (!_generator.ValidateModel(out var msg))
            {
                MessageBox.Show(msg, "Validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!int.TryParse(textBoxColumns.Text, out var colCount) || colCount < 1)
            {
                MessageBox.Show("Incorrect columns count", "Validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }            

            #endregion            

            BeforeStartPrepare();

            for (int i = 0; i < colCount; ++i)
            {
                dataGridView.Columns.Add($"Column{i}", (i + 1).ToString());
            }            

            Task.Factory.StartNew(() =>
            {
                try
                {
                    _generator.Generate(_cts.Token);
                }
                catch (OperationCanceledException)
                {
                    MessageBox.Show("Canceled", "Numerics generation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }                                
            });
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            _cts.Cancel();
        }

        private void CancellationCallback()
        {
            var callback = new Action(() =>
            {
                this.buttonStart.Enabled = true;
                this.buttonCancel.Enabled = false;
            });

            if (InvokeRequired) Invoke(callback);

            else callback();
        }

        private void InsertIntoTextBox(object sender, GenerateEventArgs e)
        {
            var args = $"{e.Value}\n";

            var insert = new Action(() => 
            {
                progressBar.Value = e.Iteration + 1;
                richTextBox.AppendText(args);
                labelGeneratedCount.Text = (e.Iteration + 1).ToString();
            });            

            if (InvokeRequired) Invoke(insert);

            else insert();
        }

        private void InsertIntoDataGrid(object sender, GenerateEventArgs e)
        {
            var insert = new Action(() =>
            {
                var colIndex = e.Iteration % (dataGridView.ColumnCount - 1);
                var rowIndex = e.Iteration / (dataGridView.ColumnCount - 1);

                if (rowIndex + 1 > dataGridView.RowCount)
                {
                    rowIndex = dataGridView.Rows.Add();
                    dataGridView.Rows[rowIndex].Cells[0].Value = rowIndex + 1;
                }

                dataGridView.Rows[rowIndex].Cells[colIndex + 1].Value = e.Value;
            });

            if (InvokeRequired) Invoke(insert);

            else insert();
        }

        private void OnGenerationFinished(object sender, EventArgs e)
        {
            if (sender is NumberGenerator generator)
            {
                var fillValues = new Action<double, double>((min, max) => 
                {
                    textBoxMinValue.Text = radioButtonBoth.Checked || radioButtonSearchMin.Checked
                    ? min.ToString() : string.Empty;

                    textBoxMaxValue.Text = radioButtonBoth.Checked || radioButtonSearchMax.Checked 
                    ? max.ToString() : string.Empty;
                });

                if (InvokeRequired) Invoke(fillValues, generator.MinValue, generator.MaxValue);

                else fillValues(generator.MinValue, generator.MaxValue);
            }

            CancellationCallback();
        }

        private void ButtonInsertFromFile_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(textBoxColumns.Text, out var colCount) || colCount < 1)
            {
                MessageBox.Show("Incorrect columns count", "Validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }           

            using (var dialog = new OpenFileDialog { Filter = "Text file (*.txt)|*.txt" })
            {
                if (dialog.ShowDialog() != DialogResult.OK) return;

                using (var reader = new StreamReader(dialog.OpenFile()))
                {
                    string line;
                    int iteration = 0;

                    dataGridView.Rows.Clear();

                    for (int i = dataGridView.ColumnCount - 1; i > 0; --i)
                        dataGridView.Columns.RemoveAt(i);

                    for (int i = 0; i < colCount; ++i)
                        dataGridView.Columns.Add($"Column{i}", (i + 1).ToString());

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (double.TryParse(line, out var value))
                        {
                            var args = new GenerateEventArgs(value, iteration);
                            InsertIntoDataGrid(null, args);
                        }

                        ++iteration;
                    }
                }
            }            
        }
    }
}