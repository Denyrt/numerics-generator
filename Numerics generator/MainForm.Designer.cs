﻿namespace Numerics_generator
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            _generator.NumberGenerated -= InsertIntoTextBox;
            _generator.NumberGenerated -= InsertIntoDataGrid;
            _generator.GenerationFinished -= OnGenerationFinished;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemClear = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxMinValue = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonBoth = new System.Windows.Forms.RadioButton();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNumCount = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.radioButtonSearchMax = new System.Windows.Forms.RadioButton();
            this.radioButtonSearchMin = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMaxValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelGeneratedCount = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.ColumnRowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxColumns = new System.Windows.Forms.TextBox();
            this.buttonInsertFromFile = new System.Windows.Forms.Button();
            this.contextMenuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox
            // 
            this.richTextBox.ContextMenuStrip = this.contextMenuStrip;
            this.richTextBox.Location = new System.Drawing.Point(12, 12);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(305, 406);
            this.richTextBox.TabIndex = 0;
            this.richTextBox.Text = "";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemClear});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(113, 28);
            // 
            // toolStripMenuItemClear
            // 
            this.toolStripMenuItemClear.Name = "toolStripMenuItemClear";
            this.toolStripMenuItemClear.Size = new System.Drawing.Size(112, 24);
            this.toolStripMenuItemClear.Text = "Clear";
            this.toolStripMenuItemClear.Click += new System.EventHandler(this.ToolStripMenuItemClear_Click);
            // 
            // textBoxMinValue
            // 
            this.textBoxMinValue.Location = new System.Drawing.Point(10, 75);
            this.textBoxMinValue.Name = "textBoxMinValue";
            this.textBoxMinValue.ReadOnly = true;
            this.textBoxMinValue.Size = new System.Drawing.Size(249, 30);
            this.textBoxMinValue.TabIndex = 1;
            this.textBoxMinValue.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonBoth);
            this.groupBox1.Controls.Add(this.buttonCancel);
            this.groupBox1.Controls.Add(this.buttonStart);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxNumCount);
            this.groupBox1.Controls.Add(this.progressBar);
            this.groupBox1.Controls.Add(this.radioButtonSearchMax);
            this.groupBox1.Controls.Add(this.radioButtonSearchMin);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxMaxValue);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxMinValue);
            this.groupBox1.Location = new System.Drawing.Point(323, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(277, 429);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Numerics setttings";
            // 
            // radioButtonBoth
            // 
            this.radioButtonBoth.AutoSize = true;
            this.radioButtonBoth.Checked = true;
            this.radioButtonBoth.Location = new System.Drawing.Point(60, 211);
            this.radioButtonBoth.Name = "radioButtonBoth";
            this.radioButtonBoth.Size = new System.Drawing.Size(123, 27);
            this.radioButtonBoth.TabIndex = 12;
            this.radioButtonBoth.TabStop = true;
            this.radioButtonBoth.Text = "Search both";
            this.radioButtonBoth.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Enabled = false;
            this.buttonCancel.Location = new System.Drawing.Point(143, 359);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(119, 30);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(13, 359);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(116, 30);
            this.buttonStart.TabIndex = 10;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 262);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 23);
            this.label3.TabIndex = 9;
            this.label3.Text = "Numerics count";
            // 
            // textBoxNumCount
            // 
            this.textBoxNumCount.Location = new System.Drawing.Point(13, 288);
            this.textBoxNumCount.Name = "textBoxNumCount";
            this.textBoxNumCount.Size = new System.Drawing.Size(249, 30);
            this.textBoxNumCount.TabIndex = 8;
            this.textBoxNumCount.Text = "10";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(13, 324);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(249, 29);
            this.progressBar.TabIndex = 7;
            // 
            // radioButtonSearchMax
            // 
            this.radioButtonSearchMax.AutoSize = true;
            this.radioButtonSearchMax.Location = new System.Drawing.Point(140, 182);
            this.radioButtonSearchMax.Name = "radioButtonSearchMax";
            this.radioButtonSearchMax.Size = new System.Drawing.Size(119, 27);
            this.radioButtonSearchMax.TabIndex = 6;
            this.radioButtonSearchMax.Text = "Search max";
            this.radioButtonSearchMax.UseVisualStyleBackColor = true;
            // 
            // radioButtonSearchMin
            // 
            this.radioButtonSearchMin.AutoSize = true;
            this.radioButtonSearchMin.Location = new System.Drawing.Point(10, 182);
            this.radioButtonSearchMin.Name = "radioButtonSearchMin";
            this.radioButtonSearchMin.Size = new System.Drawing.Size(116, 27);
            this.radioButtonSearchMin.TabIndex = 5;
            this.radioButtonSearchMin.Text = "Search min";
            this.radioButtonSearchMin.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Max value";
            // 
            // textBoxMaxValue
            // 
            this.textBoxMaxValue.Location = new System.Drawing.Point(10, 134);
            this.textBoxMaxValue.Name = "textBoxMaxValue";
            this.textBoxMaxValue.ReadOnly = true;
            this.textBoxMaxValue.Size = new System.Drawing.Size(249, 30);
            this.textBoxMaxValue.TabIndex = 3;
            this.textBoxMaxValue.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Min value";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 421);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Numbers generated:";
            // 
            // labelGeneratedCount
            // 
            this.labelGeneratedCount.AutoSize = true;
            this.labelGeneratedCount.Location = new System.Drawing.Point(185, 421);
            this.labelGeneratedCount.Name = "labelGeneratedCount";
            this.labelGeneratedCount.Size = new System.Drawing.Size(19, 23);
            this.labelGeneratedCount.TabIndex = 4;
            this.labelGeneratedCount.Text = "0";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnRowNumber});
            this.dataGridView.Location = new System.Drawing.Point(606, 12);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidth = 51;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(510, 389);
            this.dataGridView.TabIndex = 5;
            // 
            // ColumnRowNumber
            // 
            this.ColumnRowNumber.HeaderText = "№";
            this.ColumnRowNumber.MinimumWidth = 6;
            this.ColumnRowNumber.Name = "ColumnRowNumber";
            this.ColumnRowNumber.Width = 125;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(608, 414);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 23);
            this.label6.TabIndex = 9;
            this.label6.Text = "Columns:";
            // 
            // textBoxColumns
            // 
            this.textBoxColumns.Location = new System.Drawing.Point(695, 411);
            this.textBoxColumns.Name = "textBoxColumns";
            this.textBoxColumns.Size = new System.Drawing.Size(100, 30);
            this.textBoxColumns.TabIndex = 8;
            this.textBoxColumns.Text = "3";
            // 
            // buttonInsertFromFile
            // 
            this.buttonInsertFromFile.Location = new System.Drawing.Point(834, 411);
            this.buttonInsertFromFile.Name = "buttonInsertFromFile";
            this.buttonInsertFromFile.Size = new System.Drawing.Size(243, 30);
            this.buttonInsertFromFile.TabIndex = 10;
            this.buttonInsertFromFile.Text = "Insert into table from file";
            this.buttonInsertFromFile.UseVisualStyleBackColor = true;
            this.buttonInsertFromFile.Click += new System.EventHandler(this.ButtonInsertFromFile_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1128, 453);
            this.Controls.Add(this.buttonInsertFromFile);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxColumns);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.labelGeneratedCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Numerics generator";
            this.contextMenuStrip.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.TextBox textBoxMinValue;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMaxValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNumCount;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.RadioButton radioButtonSearchMax;
        private System.Windows.Forms.RadioButton radioButtonSearchMin;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemClear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelGeneratedCount;
        private System.Windows.Forms.RadioButton radioButtonBoth;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRowNumber;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxColumns;
        private System.Windows.Forms.Button buttonInsertFromFile;
    }
}

